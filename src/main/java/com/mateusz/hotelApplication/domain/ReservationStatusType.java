package com.mateusz.hotelApplication.domain;

public enum ReservationStatusType {
    NEW,IN_PROGRESS,CANCELLED,COMPLETED
}
